**Currently under heavy development and you may want to compare results with Stata.**

This repository is intended for my students in ECON407: Cross Section Econometrics at the College of William and Mary.  These functions are written to make `R` a bit more like `Stata` for things like
- Running Models
- Robust and clustered standard errors

## Installation

- Install the `devtools` package: `install.packages("devtools")` 
- Install the `econ407tools` package: `remotes::install_gitlab('robhicks/econ407tools')`.


## Example usage:

```R
# load foreign for reading stata datasets
library(foreign)
library(econ407tools)

tk.df = read.dta("https://rlhick.people.wm.edu/econ407/data/tobias_koop.dta")
tk4.df = subset(tk.df, time == 4)


#
# OLS
#

# run some regressions demonstrating straight ols, ols with robust se's, and
# cluster robust se's:
regress(ln_wage~educ + pexp + pexp2 + broken_home, data = tk.df)
regress(ln_wage~educ + pexp + pexp2 + broken_home, data = tk.df, robust=T)
regress(ln_wage~educ + pexp + pexp2 + broken_home, data = tk.df, cluster_on = c("time"))

#
# Panel Data
#

# pooled panel regression
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), model_type = "pooling")
# pooled with robust se's
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), model_type = "pooling", robust=T)
# pooled with clustered (on id) se's
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), cluster_on=c('id'), model_type = "pooling")


# fe panel regression
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), model_type = "within")
# fe with robust se's (note stata won't calculate this)
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), model_type = "within", robust=T)
# fe with clustered (on id) se's
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), cluster_on=c('id'), model_type = "within")

# re panel regression
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), model_type = "random")
# re with robust se's (note stata won't calculate this)
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), model_type = "random", robust=T)
# re with robust clustered (on id) se's
xtreg(ln_wage~educ + pexp + pexp2 + broken_home, data_frame = tk.df,
      panel_id = c('id','time'), cluster_on=c('id'), model_type = "random")


#
# Endogeneity
# 
# 2sls
#
# normal se's (matches ivregress 2sls ln_wage pexp broken_home (educ=feduc), small)
ivregress(ln_wage~educ+pexp+broken_home,~pexp+broken_home+feduc, tk.df)
# robust se's (matches ivregress 2sls ln_wage pexp broken_home (educ=feduc), small robust)
ivregress(ln_wage~educ+pexp+broken_home,~pexp+broken_home+feduc, tk.df, robust=T)
# clustered se's matches (ivregress 2sls ln_wage pexp broken_home (educ=feduc), small cluster(time))
ivregress(ln_wage~educ+pexp+broken_home,~pexp+broken_home+feduc, tk.df, cluster_on=c("time"))

```

## To do

Incorporate
1. IV GMM Regression
2. Logit and Probit
3. Heckman and Tobit
4. Conditional Logistic 
5. Write a test suite against known results
